<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Annna
 * @since Annna 1.0
 */

get_header(); ?>

<?php while (have_posts()) : the_post();
	get_template_part('partials/single');
endwhile; ?>

<?php
	$previous = get_previous_post();
	$next = get_next_post();


  if (!empty($previous) && !empty($next)): 
    $categories_p = get_the_category($previous->ID);
    $category_p = $categories_p[0]->cat_name;

    $categories_n = get_the_category($next->ID);
    $category_n = $categories_n[0]->cat_name;
  ?>

	<div class="container-fluid">
    <div class="row">
      <div class="col-sm-6 wrapper wrapper-previous wow fadeIn" style="background:url('<?php echo get_the_post_thumbnail_url($previous->ID) ?>') no-repeat center center transparent;background-size:cover;">
        <div class="overlay overlay-dark"></div>
        <a href="<?php echo get_permalink($previous->ID) ?>">
          <h6 class="wrapper-category"><?php echo $category_p; ?></h6>
          <h4><?php echo $previous->post_title ?></h4>
          <p>por <?php echo get_the_author($previous->ID) ?></p>
        </a>
      </div>

      <div class="col-sm-6 wrapper wrapper-next wow fadeIn" style="background:url('<?php echo get_the_post_thumbnail_url($next->ID) ?>') no-repeat center center transparent;background-size:cover;">
        <div class="overlay overlay-dark"></div>
        <a href="<?php echo get_permalink($next->ID) ?>">
          <h6 class="wrapper-category"><?php echo $category_n; ?></h6>
          <h4><?php echo $next->post_title ?></h4>
          <p>por <?php echo get_the_author($next->ID) ?></p>
        </a>
      </div>
    </div>
  </div>
<?php endif ?>

<?php get_footer(); ?>
