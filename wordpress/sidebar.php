<?php
/**
 * The template for the sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Annna
 * @since Annna 0.1
 */
?>

<div class="wrapper-side dark-side col-md-3 col-lg-4">
	<?php dynamic_sidebar('sidebar'); ?>
</div>
