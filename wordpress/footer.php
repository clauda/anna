<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Annna
 * @since Annna 1.0
 */
?>

 		<!-- newsletter -->
    <div class="wrapper-newsletter">
      <h3>Quer aprender mais? Mantenha-se sempre atualizado: Assine nossa newsletter.</h3>
      <form class="form-inline">
        <div class="form-group">
          <input type="email" class="form-control" id="email" placeholder="Seu email">
        </div>
        <button type="submit" class="btn">Confirm</button>
      </form>
    </div>
    <!-- /newsletter -->

    <!-- Footer -->
    <div id="footer" class="wrapper wow fadeIn">
      <div class="container">
        <div class="row">
          <div class="col-md-2 text-right">
            <?php wp_nav_menu(array(
              'menu' => 'secondary',
              'theme_location' => 'secondary',
              'menu_class' => 'list'
            )); ?>
          </div>

          <div class="col-md-4">
            <h6><?php bloginfo('name'); ?></h6>
            <p><?php bloginfo('description'); ?></p>
          </div>

          <div class="col-md-4 col-md-offset-2 text-center">
            <?php 
              if (function_exists('the_custom_logo')) {
                the_custom_logo();
              }
            ?>
            <i class="fa fa-dribbble fa-2x fa-border-circle text-center"></i> 
            <i class="fa fa-facebook fa-2x fa-border-circle text-center col-md-offset-1"></i>
            <i class="fa fa-twitter fa-2x fa-border-circle text-center col-md-offset-1"></i> 
            <i class="fa fa-apple fa-2x fa-border-circle text-center col-md-offset-1"></i>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="wrapper-numbers">
          <div class="col-md-4">
            <?php 
              $posts = wp_count_posts();
              $published = $posts->publish; ?>
            <h3><?php echo $published ?> <small>Artigos</small></h3>
          </div>
          <div class="col-md-4">
            <h3>14.023,098 <small>Hours of Reading</small></h3>
          </div>
          <div class="col-md-4">
            <h3>198 <small>Authors</small></h3>
          </div>
        </div>
      </div>
    </div>

	<?php wp_footer(); ?>
</body>
</html>
