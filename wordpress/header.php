<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Annna
 * @since Annna 0.1
 */

?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php bloginfo('description') ?>">
  <meta name="author" content="Claudia Farias">

	<title><?php bloginfo('name'); ?><?php wp_title('|>'); ?></title>

	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" /> 
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

	<?php wp_head(); ?>
</head>

<body class="layout-boxed">

	<!-- Preloader -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>

  <?php if (have_posts()):
    if (is_home()) :
      get_template_part('partials/slider');
    endif;
  endif; ?>

	<!-- Menu -->
  <div class="wrapper wrapper-navbar wrapper-no-padding">
    <nav class="navbar navbar-inverse navbar-transparent">
      <div class="container-fluid">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand font-sm" href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name'); ?></a>
        </div>

        <div id="navbar" class="collapse navbar-collapse">
           <?php 
							wp_nav_menu(array(
								'menu' => 'primary',
								'theme_location' => 'primary',
                'menu_class' => 'nav navbar-nav navbar-right'
							));
						?>
        </div>
      </div>
    </nav>
  </div>
	