<?php
/**
 * The front page template file
 *
 * @package WordPress
 * @subpackage Annna
 * @since Annna 0.1
 */

	get_header(); ?>

  <div class="col-md-12 wrapper-lightest">
    <div class="grid">

			<?php if (have_posts()) : ?>

				<?php while (have_posts()) : the_post();
					get_template_part('partials/content');
				endwhile;

			else :
				get_template_part('partials/none');
			endif; ?>

		</div>
	</div>

	<div class="clearfix"></div>
	
	<?php 
		if (!function_exists('dynamic_sidebar') || !dynamic_sidebar()) : 
			dynamic_sidebar('cta');
			dynamic_sidebar('big-home-sponsor'); 
		endif; ?>

<?php get_footer(); ?>
