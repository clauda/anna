<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage Annna
 * @since Annna 0.1
 */

get_header(); ?>

<div class="row">
  <!-- Main -->
  <div class="col-md-8 col-lg-9 wrapper-lightest">
    <div class="grid">

		<?php 
		if (have_posts()) : ?>

			<?php
			while (have_posts()) : the_post();
				get_template_part('partial/content');
			endwhile;
			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( 'Anterior', 'annna' ),
				'next_text'          => __( 'Próximo', 'annna' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'annna' ) . ' </span>',
			) );

		else :
			get_template_part('partials/none');
		endif;
		?>

		</div>
	</div>
	<?php get_sidebar(); ?>
	
</div>

<?php get_footer(); ?>
