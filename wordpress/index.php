<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Annna
 * @since Annna 0.1
 */

	get_header(); ?>

<div class="container-fluid wrapper wrapper-default wrapper-no-padding" id="page">
  <div class="col-md-9 col-lg-8">

		<?php if (have_posts()) : ?>

    	<div class="grid">
				<?php while (have_posts()) : the_post();
					get_template_part('partials/content');
				endwhile; ?>
			</div>

			<?php 
				else:
					get_template_part('partials/none');
				endif; ?>
	</div>

	<?php get_sidebar(); ?>
</div>


<?php 
	if (!function_exists('dynamic_sidebar') || !dynamic_sidebar()) :
  	dynamic_sidebar('cta');
  endif;

	if (have_posts()):
		if (!is_front_page()) :
			get_template_part('partials/slider');
		endif;
endif; ?>

<?php get_footer(); ?>
