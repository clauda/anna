<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Annna
 * @since Annna 0.1
 */

$categories = get_the_category();
$category = $categories[0]->cat_name;

?>

<div class="container-fluid wrapper-no-padding wrapper-head">
  <div class="col-md-9 col-lg-8 wrapper-title">
    <h6 class="wrapper-category"><?php the_category() ?></h6>
    <h4><?php the_title() ?></h4>
    <p><?php the_author() ?> em <?php the_time() ?></p>
  </div>

  <div class="wrapper-side col-md-3 col-lg-4 ad" id="js-ad">
    <?php 
      if (!function_exists('dynamic_sidebar') || !dynamic_sidebar()) :
        dynamic_sidebar('top-sidebar-sponsor');
      endif; ?>
  </div>
</div>

<div class="container-fluid wrapper wrapper-default wrapper-no-padding" id="page">

  <div class="col-md-9 col-lg-8 wrapper-no-padding col-border-right">
    <article class="wrapper-md">
    	<img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="img-responsive" /><br/>
    
    	<?php the_content(); ?>
		</article>

		<div class="wrapper-md">
      <div class="col-sm-12 wrapper-author">
        <h6><?php the_author(); ?></h6>
        <p><?php the_author_meta('description'); ?></p>
        <a href="http://twitter/<?php the_author_meta('twitter'); ?>" rel="author">
          @<?php the_author_meta('twitter') ?>
        </a>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="row wrapper-no-padding">
      <div class="col-sm-12">
        <div class="wrapper-comments">
          <!-- Disqus -->
        </div>
      </div>
    </div>
  </div>

  <?php get_sidebar(); ?>
</div>
