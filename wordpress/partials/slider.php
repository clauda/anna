<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Annna
 * @since Annna 0.1
 */

  $slides = new WP_Query(array('tag' => 'slider', 'posts_per_page' => 5));
?>

 <div class="wrapper-75vh wrapper-darkest wrapper-overflow-hidden wrapper-gradient4" id="start">

  <div id="generic" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      
      <?php 
        while ($slides->have_posts()) : $slides->the_post(); ?>

        <div class="item <?php if ($slides->current_post == 0) { echo "active"; }?>">
          <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" />
          <div class="overlay overlay-dark"></div>
          <div class="carousel-caption">
            <h6 class="category"><?php the_category(); ?></h6>
            <h1 class="headline"><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h1>
            <p class="lead"><?php the_excerpt(); ?></p>
            <a class="btn btn-inverse btn-lg hiddex-xs" href="<?php the_permalink();?>">Ler na Íntegra</a>
          </div>
        </div>
        
      <?php endwhile;
       wp_reset_postdata(); ?>

      <!-- Controls -->
      <a class="left carousel-control" href="#generic" role="button" data-slide="prev">
        <span aria-hidden="true" class="fa fa-chevron-left"></span>
        <span class="sr-only">Anterior</span>
      </a>
      <a class="right carousel-control" href="#generic" role="button" data-slide="next">
        <span aria-hidden="true" class="fa fa-chevron-right"></span>
        <span class="sr-only">Próximo</span>
      </a>

    </div>
  </div>
</div>

