<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 * @package WordPress
 * @subpackage Annna
 * @since Annna 0.1
 */
?>

<section class="no-results not-found">
	<h3><?php _e('Nada aqui! ;/', 'annna'); ?></h3>

	<?php if (is_home() && current_user_can('publish_posts')) : ?>

		<h6><?php printf( __('Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'annna' ), esc_url(admin_url('post-new.php'))); ?></h6>

	<?php elseif (is_search()) : ?>

		<h6><?php _e('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'annna'); ?></h6>

		<?php get_search_form(); ?>

	<?php else : ?>

		<h6><?php _e('It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'annna'); ?></h6>
		
		<?php 
			get_search_form(); 

		endif;
	?>

</section>
