<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Annna
 * @since Annna 0.1
 */
?>

<div class="grid-item <?php echo get_post_format($post->ID); ?>">
  <div class="wrapper-article wow fadeIn">
    <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="img-responsive" />
    <div class="article-caption">
      <h6 class="wrapper-category"><?php the_category() ?></h6>
      <h5>
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
          <?php the_title(); ?>
        </a>
      </h5>
    </div>
  </div>
</div>
