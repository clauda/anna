<?php
/**
 * Annna Theme functions and definitions
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Annna
 * @since Annna 0.1
 */

/**
 * Annna only works in WordPress 4.4 or later.
 */

if (version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<')) {
	require get_template_directory() . '/inc/back-compat.php';
}

if (!function_exists('annna_setup')):
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * Create your own annna_setup() function to override in a child theme.
 *
 * @since Annna 0.1
 */
function annna_setup() {
	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	// add_theme_support('title-tag');

	/*
	 * Enable support for custom logo.
	 *
	 */
	add_theme_support('custom-logo', array(
		'height'      => 360,
		'width'       => 125,
		'flex-height' => true,
	));

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support('post-thumbnails');
	set_post_thumbnail_size(1200, 9999);

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __('Primary Menu', 'annna'),
		'seconday'  => __('Footer Menu', 'annna'),
	));

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support('html5', array(
		'search-form',
		'caption'
	));

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support('post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'status',
		'audio',
		'chat',
	));

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	// add_editor_style(array('css/editor-style.css', annna_fonts_url()));

	// Indicate widget sidebars can use selective refresh in the Customizer.
	// add_theme_support('customize-selective-refresh-widgets');
}
endif; // annna_setup
add_action('after_setup_theme', 'annna_setup');

/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 *
 */

function annna_contactmethods($contactmethods) {
  $contactmethods['twitter'] = 'Twitter';
  $contactmethods['github'] = 'Github';
  return $contactmethods;
}
add_filter('user_contactmethods', 'annna_contactmethods', 10, 1);

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 */
function annna_widgets_init() {
	register_sidebar(array(
		'name'          => __('Sidebar', 'annna'),
		'id'            => 'sidebar',
		'description'   => __('Add widgets here to appear in your sidebar.', 'annna')
	));

	register_sidebar(array(
		'name'          => __('Post Sponsor', 'annna'),
		'id'            => 'top-sidebar-sponsor',
		'description'   => __('No topo do sidebar da página de posts.', 'annna'),
		'before_widget' => '<div id="%1$s">',
		'after_widget' => '</div>'
	));

	register_sidebar(array(
		'name'          => __('Big Home Sponsor', 'annna'),
		'id'            => 'big-home-sponsor',
		'description'   => __('Aparece como um grande bloco na página inicial', 'annna'),
		'before_widget' => '<div id="%1$s">',
		'after_widget' => '</div>'
	));

	register_sidebar(array(
		'name'          => __('CTA', 'annna'),
		'id'            => 'cta',
		'description'   => __('Aparece como um grande bloco', 'annna'),
		'before_widget' => '<div id="%1$s">',
		'after_widget' => '</div>'
	));
}
add_action('widgets_init', 'annna_widgets_init');

/**
 * Enqueues scripts and styles.
 * @since Annna 1.0
 */
function annna_scripts() {
	wp_enqueue_script('annna-jquery', get_template_directory_uri() . '/js/jquery.min.js',  array(), '20160605', true);
	wp_enqueue_script('annna-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '20160605', true);
	wp_enqueue_script('annna-masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array(), '20160605', true);
	wp_enqueue_script('annna-prism', get_template_directory_uri() . '/js/prism.min.js', array(), '20160605', true);
	wp_enqueue_script('annna-wow', get_template_directory_uri() . '/js/wow.min.js', array(), '20160605', true);
	wp_enqueue_script('annna-app', get_template_directory_uri() . '/js/app.min.js', array(), '20160605', true);
}
add_action('wp_enqueue_scripts', 'annna_scripts');

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function annna_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	840 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px';

	if ( 'page' === get_post_type() ) {
		840 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
	} else {
		840 > $width && 600 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px';
		600 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
	}

	return $sizes;
}
add_filter('wp_calculate_image_sizes', 'annna_content_image_sizes_attr', 10 , 2);

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails
 *
 * @param array $attr Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size Registered image size or flat array of height and width dimensions.
 * @return string A source size value for use in a post thumbnail 'sizes' attribute.
 */
function annna_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( 'post-thumbnail' === $size ) {
		is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';
		! is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';
	}
	return $attr;
}
add_filter('wp_get_attachment_image_attributes', 'annna_post_thumbnail_sizes_attr', 10 , 3);

/**
* Extend Recent Posts Widget 
* Adds different formatting to the default WordPress Recent Posts Widget
*/
Class Annna_Recent_Posts_Widget extends WP_Widget_Recent_Posts {

	function widget($args, $instance) {
		extract( $args );
		
		if(empty( $instance['number'] ) || ! $number = absint( $instance['number'] ))
			$number = 10;
					
		$results = new WP_Query( apply_filters( 'widget_posts_args', array( 'posts_per_page' => $number, 'no_found_rows' => true, 'post_status' => 'publish', 'ignore_sticky_posts' => true ) ) );
		if( $results->have_posts() ) :

			
			echo $before_widget; ?>
			<section class="widget wrapper-secondary">
				<ul>
					<?php while($results->have_posts()) : $results->the_post(); 
						$categories = get_the_category();
						$category = $categories[0]->cat_name;
					?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
	            <h6 class="wrapper-category"><?php echo $category; ?></h6>
	            <h4><?php the_title(); ?></h4>
	          </a>
					</li>
					<?php endwhile; ?>
				</ul>
			</section>
			 
			<?php
				echo $after_widget;
				wp_reset_postdata();
				endif;
	}
}

// Change before_widget damn li
function annna_widget_func($params) {
  $params[0]['before_widget'] = '<div id="%1$s" class="widget %2$s">';
  $params[0]['after_widget'] = '</div>';
  return $params;
}
add_filter('dynamic_sidebar_params', 'annna_widget_func');

function annna_recent_widget_registration() {
  unregister_widget('WP_Widget_Recent_Posts');
  register_widget('Annna_Recent_Posts_Widget');
}
add_action('widgets_init', 'annna_recent_widget_registration');