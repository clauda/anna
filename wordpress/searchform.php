<?php
/**
 * Template for displaying search forms in Twenty Sixteen
 *
 * @package WordPress
 * @subpackage Annna
 * @since Annna 0.1
 */
?>

<section id="search" class="wrapper-search">
  <form role="search" method="get" class="form-inline" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="input-group">
		  <input type="search" class="form-control" placeholder="Pesquisar" value="<?php echo get_search_query(); ?>" name="s" />
      <div class="input-group-btn">
        <button type="submit" class="btn btn-default">
          <i class="fa fa-search"></i>
        </button>
      </div>
    </div>
  </form>
</section>
