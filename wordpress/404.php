<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Annna
 * @since Annna 0.1
 */

get_header(); ?>

	<div class="container-fluid wrapper wrapper-default wrapper-no-padding" id="page">
    <!-- Main -->
    <div class="col-md-8 col-lg-9">

			<section class="error-404 not-found">
				<header class="page-header">
					<h3 class="page-title">
						<?php _e('Oops! That page can&rsquo;t be found.', 'annna'); ?>
					</h3>
				</header><!-- .page-header -->

				<div class="page-content">
					<p>
						<?php _e('It looks like nothing was found at this location. Maybe try a search?', 'annna'); ?>
					</p>

					<?php get_search_form(); ?>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</div>

		<?php get_sidebar('slider'); ?>

	</div><!-- .content-area -->
	
<?php get_template_part('slider'); ?>

<?php get_footer(); ?>
