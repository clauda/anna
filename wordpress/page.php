<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Annna
 * @since Annna 0.1
 */

get_header(); ?>

<div class="container-fluid wrapper wrapper-default wrapper-no-padding" id="page">
  <div class="col-md-9 col-lg-8">

			<?php if (have_posts()): ?>
				<div class="grid">
					<?php while (have_posts()) : the_post();
						get_template_part('partials/content');
					endwhile;
				</div>

				// Previous/next page navigation.
				the_posts_pagination( array(
					'prev_text'          => __('Anterior', 'annna'),
					'next_text'          => __('Próximo', 'annna'),
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . __('Page', 'annna' ) . ' </span>',
				));

			else:
				get_template_part('partials/none');
			endif; ?>

	</div>
	
	<?php get_sidebar(); ?>
</div>

<?php 
	if (!function_exists('dynamic_sidebar') || !dynamic_sidebar()) :
  	dynamic_sidebar('cta');
  endif; ?>

<?php if (have_posts()):
	if (!is_front_page()) :
		get_template_part('partials/slider');
	endif;
endif; ?>

<?php get_footer(); ?>
