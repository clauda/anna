### Anna Theme HTML/CSS


Install npm, Bower, SASS and Gulp, then:

`$ npm install`

`$ bower install`

Compile SCSS into CSS maually

`$ gulp sass`

Watch file changes and compile SCSS fils into CSS automatically on save

`$ gulp watch`

Clean up folders, minifies images and CSS files and move everything to the /dist folder

`$ gulp dist`